var parameter = new URL(window.location.href).searchParams.get("type");
var remote_host = "http://localhost:5000"

if (parameter != "circle" && parameter != "sharp") {
    document.getElementById("error").innerHTML =
        '<form action="">' +
        'First name:<br>' +
        '<input type="text" id="type" name="type" value="circle or sharp"><br>' +
        '<input type="submit" value="Submit">' +
        '</form>';

} else {
    document.getElementById("error").innerHTML = '<input type="button" value="new game" onClick="window.location.reload()">';

    var map = []
    var game = new Phaser.Game(350, 350, Phaser.AUTO, 'phaser-example', { preload: preload, create: create });
    var sharp = parameter == "sharp" ? true : false;
    var used = [0]
    var used1 = ["empty", "empty", "empty", "empty", "empty", "empty", "empty", "empty", "empty"]
    var i = 0;
    var id = createGame();

    function preload() {
        this.load.image('map', 'assets/map.jpg');
        this.load.image('piece', 'assets/piece.jpg');
        this.load.image('circle', 'assets/circle.jpg');
        this.load.image('sharp', 'assets/sharp.jpg');
    }

    function create() {
        game.stage.backgroundColor = '#ffffff';

        var image = this.add.sprite(80, 70, 'piece');
        image.inputEnabled = true;
        image.events.onInputDown.add(function () { change(1, 80, 70) }, this);

        image = this.add.sprite(80, 141, 'piece');
        image.inputEnabled = true;
        image.events.onInputDown.add(function () { change(4, 80, 141) }, this);

        image = this.add.sprite(80, 212, 'piece');
        image.inputEnabled = true;
        image.events.onInputDown.add(function () { change(7, 80, 212) }, this);

        image = this.add.sprite(161, 70, 'piece');
        image.inputEnabled = true;
        image.events.onInputDown.add(function () { change(2, 161, 70) }, this);

        image = this.add.sprite(161, 141, 'piece');
        image.inputEnabled = true;
        image.events.onInputDown.add(function () { change(5, 161, 141) }, this);

        image = this.add.sprite(161, 212, 'piece');
        image.inputEnabled = true;
        image.events.onInputDown.add(function () { change(8, 161, 212) }, this);

        image = this.add.sprite(242, 70, 'piece');
        image.inputEnabled = true;
        image.events.onInputDown.add(function () { change(3, 242, 70) }, this);

        image = this.add.sprite(242, 141, 'piece');
        image.inputEnabled = true;
        image.events.onInputDown.add(function () { change(6, 242, 141) }, this);

        image = this.add.sprite(242, 212, 'piece');
        image.inputEnabled = true;
        image.events.onInputDown.add(function () { change(9, 242, 212) }, this);

        if(!sharp) {
            value = updateRobotOnServer(id, !sharp)
            robotUpdateLocally(value, !sharp)
        }
    }

    function change(nr, x, y, sendRequest) {
        if (!used.includes(nr)) {
            if(someoneWon())
                return;

            if (sharp) {
                if (parameter != "sharp")
                    return;

                game.add.sprite(x + 5, y + 5, 'sharp').scale.setTo(0.9, 0.9);
                myUpdate(nr, false)
                playerUpdate(id, nr)
                
                if(someoneWon())
                return;

                value = updateRobotOnServer(id, nr, !sharp)
                robotUpdateLocally(value, sharp)
            } else {
                if (parameter != "circle")
                    return;

                game.add.sprite(x + 5, y + 5, 'circle').scale.setTo(0.9, 0.9);
                myUpdate(nr, true)
                playerUpdate(id, nr)

                if(someoneWon())
                    return;

                value = updateRobotOnServer(id, !sharp)
                robotUpdateLocally(value, sharp)
            }
            
            if(someoneWon())
                return;
        }
    }

    function someoneWon() {

        if (checkCoordinatesForSharp(used1, 1, 2, 3))
            return true
        if (checkCoordinatesForSharp(used1, 4, 5, 6))
            return true
        if (checkCoordinatesForSharp(used1, 7, 8, 9))
            return true
        if (checkCoordinatesForSharp(used1, 1, 4, 7))
            return true
        if (checkCoordinatesForSharp(used1, 2, 5, 8))
            return true
        if (checkCoordinatesForSharp(used1, 3, 6, 9))
            return true
        if (checkCoordinatesForSharp(used1, 1, 5, 9))
            return true 
        if (checkCoordinatesForSharp(used1, 7, 5, 3))
            return true

        if (checkCoordinatesForCircle(used1, 1, 2, 3))
            return true
        if (checkCoordinatesForCircle(used1, 4, 5, 6))
            return true
        if (checkCoordinatesForCircle(used1, 7, 8, 9))
            return true
        if (checkCoordinatesForCircle(used1, 1, 4, 7))
            return true
        if (checkCoordinatesForCircle(used1, 2, 5, 8))
            return true
        if (checkCoordinatesForCircle(used1, 3, 6, 9))
            return true
        if (checkCoordinatesForCircle(used1, 1, 5, 9))
            return true 
        if (checkCoordinatesForCircle(used1, 7, 5, 3))
            return true

        return false
    }

    function checkCoordinatesForSharp(map, c1, c2, c3) {
        if(map[c1-1] == "sharp")
            if(map[c2-1] == "sharp")
                if(map[c3-1] == "sharp") {
                    document.getElementById("winner").innerHTML = '<center><h1>SHARP WON !';
                    return true;
                }
        return false;
    }

    function checkCoordinatesForCircle(map, c1, c2, c3) {
        if(map[c1-1] == "circle")
            if(map[c2-1] == "circle")
                if(map[c3-1] == "circle") {
                    document.getElementById("winner").innerHTML = '<center><h1>CIRCLE WON !';
                    return true;
                }
        return false;
    }

    function playerUpdate(myId, myNr) {
        var arr = { id: myId, nr: myNr };
        $.ajax({
            url: remote_host + "/learn",
            type: 'PUT',
            data: JSON.stringify(arr),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: false,
        });
    }

    function robotUpdateLocally(value, sharp) {
        myType = sharp ? 'sharp' : 'circle'
        console.log(value)

        if (value == 1)
            game.add.sprite(80, 70, myType).scale.setTo(0.9, 0.9);
        if (value == 4)
            game.add.sprite(80, 141, myType).scale.setTo(0.9, 0.9);
        if (value == 7)
            game.add.sprite(80, 212, myType).scale.setTo(0.9, 0.9);
        if (value == 2)
            game.add.sprite(161, 70, myType).scale.setTo(0.9, 0.9);
        if (value == 5)
            game.add.sprite(161, 141, myType).scale.setTo(0.9, 0.9);
        if (value == 8)
            game.add.sprite(161, 212, myType).scale.setTo(0.9, 0.9);
        if (value == 3)
            game.add.sprite(242, 70, myType).scale.setTo(0.9, 0.9);
        if (value == 6)
            game.add.sprite(242, 141, myType).scale.setTo(0.9, 0.9);
        if (value == 9)
            game.add.sprite(242, 212, myType).scale.setTo(0.9, 0.9);

        myUpdate(value, sharp ? false : true)
    }

    function updateRobotOnServer(myId, isCircle) {
        var value
        var arr = { id: myId};
        $.ajax({
            url: remote_host + "/learn",
            type: 'POST',
            data: JSON.stringify(arr),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: false,
            success: function (msg) {
                value = JSON.stringify(msg)
            },
            error: function (msg) {
                value = JSON.stringify(msg)

            }
        });

        return $.parseJSON(value)
    }

    function createGame() {
        var value
        $.ajax({
            url: remote_host + "/learn",
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: false,
            success: function (msg) {
                value = JSON.stringify(msg)
            },
            error: function (msg) {
                value = JSON.stringify(msg)

            }
        });

        return $.parseJSON(value)
    }

    function myUpdate(nr, form) {
        used1[nr-1] = form ? "circle" : "sharp"
        used[i++] = nr
        sharp = form;
    }
}
